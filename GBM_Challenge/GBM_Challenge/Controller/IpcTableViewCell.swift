import UIKit

class IpcTableViewCell: UITableViewCell {
  
  @IBOutlet weak var priceLabel: UILabel!
  
  @IBOutlet weak var dateLabel: UILabel!
  
  @IBOutlet weak var percentageLabel: UILabel!
  
  @IBOutlet weak var volumeLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.selectionStyle = .none
  }
  
}
