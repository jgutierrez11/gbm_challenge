import UIKit
import LocalAuthentication

class LoginViewController: UIViewController {
  
  @IBOutlet weak var errorLabel: UILabel!
  
  @IBAction func loginButtonPress(_ sender: UIButton) {
    errorLabel.text = ""
    auth()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    errorLabel.text = ""
  }
  
  fileprivate func auth () {
    let localizedReason = "Para mayor seguridad, necesitamos que te identifiques"
    var evalError: NSError?
    let authContext = LAContext()
    
    if authContext.canEvaluatePolicy(
      .deviceOwnerAuthenticationWithBiometrics,
      error: &evalError) {
      authContext.evaluatePolicy(
        .deviceOwnerAuthenticationWithBiometrics,
        localizedReason: localizedReason) { (success, error) in
          if success {
            let mainVC = MainTabBarViewController(nibName: "MainTabBarView", bundle: nil)
            self.show(mainVC, sender: nil)
          } else {
            if let err = error {
              DispatchQueue.main.async {
                self.errorLabel.text = err.localizedDescription
              }
            }
          }
      }
    } else {
      if let error = evalError {
        errorLabel.text = error.localizedDescription
      }
    }
  }
  
}
