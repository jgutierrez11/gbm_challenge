import UIKit

class ListViewController: UITableViewController {
  private let refreshingControl = UIRefreshControl()
  private var timer = Timer()
  private let intervalToRefresh = TimeInterval(3)
  
  var ipcList = [Ipc]() {
    didSet {
      DispatchQueue.main.async {
        print("Reloading")
        self.tableView.reloadData()
      }
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = "Lista"
    initTableView()
    fetchData()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    scheduledTask()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    timer.invalidate()
  }
  
  fileprivate func scheduledTask() {
    timer = Timer.scheduledTimer(
      timeInterval: intervalToRefresh,
      target: self,
      selector: #selector(self.fetchData),
      userInfo: nil,
      repeats: true
    )
  }
  
  @objc func fetchData() {
    let api = Api()
    api.graphData(company: "IPC", latestFirst: true, completion: {
      (inner) in
      do {
        self.ipcList = try inner()
        self.hideRefresher()
      } catch _ {
        self.hideRefresher()
      }
    })
  }
  
  fileprivate func hideRefresher() {
    DispatchQueue.main.async {
      if ((self.refreshControl?.isRefreshing) != nil) {
        self.refreshControl?.endRefreshing()
      }
    }
  }
}

extension ListViewController {
  
  fileprivate func initTableView() {
    let cellNib = UINib(nibName: "IpcTableViewCell", bundle: nil)
    tableView.register(cellNib, forCellReuseIdentifier: "ipcCell")
    tableView.tableFooterView = UIView()
    tableView.estimatedRowHeight = 172
    tableView.rowHeight = 172
    tableView.separatorColor = UIColor(red:0.93, green:0.93, blue:0.93, alpha:1.00)
    
    initRefreshControl()
  }
  
  fileprivate func initRefreshControl() {
    refreshControl?.attributedTitle = NSAttributedString(string: "Actualizando información")
    tableView.refreshControl = refreshingControl
    self.refreshControl?.addTarget(self,
                                   action: #selector(self.fetchData),
                                   for: UIControlEvents.valueChanged)
    self.refreshControl?.beginRefreshing()
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "ipcCell", for: indexPath) as! IpcTableViewCell
    cell.accessoryType = .none
    
    if (ipcList.count > 0) {
      let stringHelper = StringHelper()
      let ipc = ipcList[indexPath.row]
      cell.priceLabel.text = stringHelper.formattedPrice(ipc.price)
      cell.volumeLabel.text = String(format:"%.3f", ipc.volume)
      cell.percentageLabel.text = "\(String(format:"%.3f", ipc.percentage))%"
      cell.dateLabel.text = stringHelper.formattedDate(ipc.date)
    }
    return cell
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return ipcList.count
  }
}
