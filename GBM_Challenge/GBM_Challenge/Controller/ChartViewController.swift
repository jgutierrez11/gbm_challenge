import UIKit
import Charts

class ChartViewController: UIViewController {
  
  @IBOutlet weak var chartView: LineChartView!
  
  var ipcList = [Ipc]() {
    didSet {
      DispatchQueue.main.async {
        print("Updating Chart")
        self.updateChart()
      }
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = "Gráfica"
    fetchData()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
  }
  
  fileprivate func updateChart() {
    var lineChart = [ChartDataEntry]()
    var i = 0
    
    for ipc in ipcList {
      let cde = ChartDataEntry(x: Double(i), y: ipc.price)
      i += 1;
      lineChart.append(cde)
    }
    
    let line1 = LineChartDataSet(values: lineChart, label: "IPC")
    line1.circleHoleRadius = CGFloat(0.5)
    line1.circleColors = [NSUIColor.red]
    line1.circleRadius = CGFloat(0.5)
    line1.colors = [NSUIColor.blue]
    
    let chartData = LineChartData()
    chartData.addDataSet(line1)
    chartView.data = chartData
    chartView.chartDescription?.text = "GBM"
    
    let xAxis = chartView.xAxis
    let customAxisFormatter = CustomBottomAxisFormatter(usingValues: ipcList)
    xAxis.valueFormatter = customAxisFormatter
  }
  
  fileprivate func fetchData() {
    let api = Api()
    api.graphData(company: "IPC", completion: {
      (inner) in
      do {
        self.ipcList = try inner()
      } catch _ {
        print("Algo salió mal")
      }
    })
  }
}
