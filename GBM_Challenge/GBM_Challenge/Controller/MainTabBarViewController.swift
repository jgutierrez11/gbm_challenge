import UIKit

class MainTabBarViewController: UITabBarController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    initTabBar()
  }
  
  fileprivate func initTabBar() {
    let chartVC = ChartViewController(nibName: "ChartView", bundle: nil)
    let listVC = ListViewController(nibName: "ListView", bundle: nil)
    
    listVC.title = "Lista"
    listVC.tabBarItem = UITabBarItem(title: "Lista",
                                     image: UIImage(named: "list"),
                                     selectedImage: UIImage(named: "list"))
    chartVC.title = "Gráfica"
    chartVC.tabBarItem = UITabBarItem(title: "Gráfica",
                                      image: UIImage(named: "chart"),
                                      selectedImage: UIImage(named: "chart"))
    
    self.tabBar.tintColor = UIColor(red:0.13, green:0.28, blue:0.49, alpha:1.00)
    
    let controllers = [chartVC, listVC];
    self.setViewControllers(controllers, animated: false)
    self.viewControllers = controllers.map{
      UINavigationController(rootViewController: $0)
    }
  }
}
