import Foundation

class StringHelper {
  public func formattedPrice(_ price: Double) -> String {
    let currencyFormatter = NumberFormatter()
    currencyFormatter.usesGroupingSeparator = true
    currencyFormatter.numberStyle = .currency
    currencyFormatter.locale = Locale.current
    return currencyFormatter.string(from: NSNumber(value: price))!
  }
  
  public func formattedDate(_ date: String) -> String {
    let formatter = DateFormatter()
    formatter.locale = Locale(identifier: "en_US_POSIX")
    formatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss.SSxxx"
    formatter.calendar = Calendar(identifier: .gregorian)
    
    if let date = formatter.date(from: date) {
      formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
      return formatter.string(from: date)
    }
    return ""
  }
}
