import Foundation
import Charts

class CustomBottomAxisFormatter: NSObject, IAxisValueFormatter {
  private var values: [Ipc]?
  
  lazy private var dateFormatter: DateFormatter = {
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale.current
    dateFormatter.dateStyle = .none
    dateFormatter.timeStyle = .short
    return dateFormatter
  }()
  
  convenience init(usingValues values: [Ipc]) {
    self.init()
    self.values = values
  }
  
  func stringForValue(_ value: Double, axis: AxisBase?) -> String {
    let index = Int(value)
    
    let formatter = DateFormatter()
    formatter.locale = Locale(identifier: "en_US_POSIX")
    formatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss.SSxxx"
    formatter.calendar = Calendar(identifier: .gregorian)
    
    let date = formatter.date(from: values![index].date)
    
    return dateFormatter.string(from: date!)
  }
}
