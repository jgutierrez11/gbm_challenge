import Foundation

struct WsResponse : Decodable {
  let result: Bool
  let resultObj: [Ipc]
}
