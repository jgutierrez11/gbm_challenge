import Foundation

public class Api {
  let BASE_URL = "https://www.gbm.com.mx"
  let MARKETS = "/Mercados"
  let GET_GRAPH_DATA = "/ObtenerDatosGrafico?empresa="
  
  func graphData(company: String,
                 latestFirst: Bool = false,
                 completion: @escaping (_ inner: () throws -> ([Ipc])) -> ()) {
    let url = URL(string: BASE_URL + MARKETS + GET_GRAPH_DATA + company)
    
    let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
      if error != nil {
        print(error!)
        completion({return [Ipc]()})
      } else {
        do {
          let responseObject = try JSONDecoder().decode(WsResponse.self, from: data!)
          if latestFirst {
            completion({return responseObject.resultObj.sorted(by: { $0.date > $1.date })})
          } else {
            completion({return responseObject.resultObj})
          }
        } catch {
          print("JSON Processing failed")
          completion({return [Ipc]()})
        }
      }
    }
    
    task.resume()
  }
}
