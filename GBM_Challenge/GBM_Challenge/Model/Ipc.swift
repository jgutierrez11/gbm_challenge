import Foundation

struct Ipc : Decodable {
  let date: String
  let price: Double
  let percentage: Double
  let volume: Double
  
  enum CodingKeys : String, CodingKey {
    case date = "Fecha"
    case price = "Precio"
    case percentage = "Porcentaje"
    case volume = "Volumen"
  }
}
